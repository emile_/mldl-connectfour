import numpy as np 
import pandas as pd
import tensorflow as tf
from connectfour import Game
from sklearn.model_selection import train_test_split

SIZE = 7*6

# data = np.load('c4-small.npy')
X = np.load('board_input.npy')
y = np.load('move_output.npy')

X_train = np.expand_dims(X, -1)

#model.add(tf.keras.Input(shape=(6,7,1)))
model = tf.keras.Sequential()
#model.add(tf.keras.layers.Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(6,7,1)))
#model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
#model.add(tf.keras.layers.Conv2D(64, kernel_size=(3, 3), activation='relu'))
#model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))

model.add(tf.keras.layers.Dense(60, input_shape=(6,7,1)))
model.add(tf.keras.layers.Dense(80, activation='relu'))
model.add(tf.keras.layers.Dense(80, activation='relu'))
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(7, activation='softmax'))
#model.add(tf.keras.layers.Conv2D(100, (4,4), input_shape=(
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.summary()
model.fit(X_train, y_train, validation_split=0.3, epochs=100, batch_size=50)

#print(data[123])
#print(np.argmax(model.predict(np.array([0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0]).reshape(-1, SIZE))))
#print(np.argmax(model.predict(data[123,:SIZE].reshape(-1, SIZE))))
#print(data[10])
#print(np.argmax(model.predict(data[10,:SIZE].reshape(-1, SIZE))))

model.save('kerasmodel')
