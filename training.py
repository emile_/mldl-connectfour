import numpy as np 
import pandas as pd
import tensorflow as tf
from connectfour import Game

SIZE = 7*6
data = np.load('c4.npy')
print(data.shape[0])
X = []
y = []
weights = []

for i in range(0, data.shape[0]):
    y.append(tf.keras.utils.to_categorical(data[i,-2], num_classes=7))
    weights.append(1+(data[i, -1]))
    X.append(data[i, :SIZE])

X_train = np.array(X)
y_train = np.array(y)
sample_weights = np.array(weights)

model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(60, input_shape=(SIZE,)))
model.add(tf.keras.layers.Dense(80, activation='relu'))
model.add(tf.keras.layers.Dense(80, activation='relu'))
model.add(tf.keras.layers.Dense(7, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy', tf.keras.metrics.TopKCategoricalAccuracy(k=2)])
model.summary()

model.fit(X_train, y_train, validation_split=0.2, sample_weight=sample_weights, epochs=100, batch_size = 100)

model.save('model')
