from connectfour import Game
import numpy as np 
import pandas as pd
import tensorflow as tf
from tensorflow import keras

SIZE = 7*6
model = keras.models.load_model('model')
wins = 0
losses = 0
draws = 0
first = -1
for n in range(0, 500):
    game = Game()
    while game.status is None:
        if first<0:
            prediction = model.predict(np.hstack(game.board).reshape(-1, SIZE))
            move = np.argmax(prediction)
        else:
            move, p = game.smart_action(-1, legal_only=True, n=50)
        game.play_move(-1, move)
            
        if game.status is not None:
            break

        if first>0:
            prediction = model.predict(np.hstack(game.board).reshape(-1, SIZE) * -1)
            move = np.argmax(prediction)
        else:
            move, p = game.smart_action(1, legal_only=True, n=50)
        game.play_move(1, move)

    if game.status == first:
        wins+=1
    elif game.status == -first:
        losses+=1
    else:
        draws+=1
    print("game: ",n, " total wins: ", wins, "total losses: ", losses, "total draws: ", draws, " win-rate: ", wins/(wins+losses))
    first *=-1

