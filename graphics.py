from connectfour import Game
import numpy as np 
import pandas as pd
import tensorflow as tf
from tensorflow import keras
import pygame

model = keras.models.load_model('model')
SIZE = 7*6
running = True

colors = [(255,10,80), (0,0,0), (10,80,255)]

pygame.init()
screen = pygame.display.set_mode([550, 500])
font = pygame.font.Font('freesansbold.ttf', 70)

game = Game()
prediction = model.predict(np.hstack(game.board).reshape(-1, SIZE))
move = np.argmax(prediction)
game.play_move(-1, move)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            if game.status is not None:
                game = Game()
            else:
                pos = pygame.mouse.get_pos()
                col = int(round((pos[0]-50)/70))
                game.play_move(1, col)
                if game.status is None:
                    prediction = model.predict(np.hstack(game.board).reshape(-1, SIZE))
                    move = np.argmax(prediction)
                    game.play_move(-1, move)

    screen.fill((255, 255, 255))
    for ri, row in enumerate(np.flipud(game.board)):
        for i,x in enumerate(row):
            pygame.draw.circle(screen, colors[x+1], (50+i*70, 50+ri*70), 30)
    if game.status == -1:
        text = font.render('You Lose...', False, (255,40,40))
        screen.blit(text, (100,50))
    if game.status == 1:
        text = font.render('You Win!', False, (40,255,40))
        screen.blit(text, (100,50))
    pygame.display.flip()
pygame.quit()
