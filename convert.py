import numpy as np
import pandas as pd
from connectfour import Game, diagonals, antidiagonals
import copy

def check_gap_consecutive(board, consecutive=4):
    for row in board:
        count = 1
        for col in range(1, len(row)):
            if row[col - 1] == row[col]:
                count += 1
            elif col < len(row) -2 and row[col-1] == row[col+1] and row[col] == 0:
                count += 0
            else:
                count = 1
            if count == consecutive and row[col] != 0:
                return row[col]
    return 0

def as_board(moves):
    states = []
    labels = []

    max_idx = 0
    game = Game()
    for _,i in moves.iterrows():
        idx, player, move, winner = i['idx'], i['player'], i['move'], i['winner']
        if player == winner:
            max_idx = max(idx, max_idx)
            labels.append([0,0,0])
            if player > 0:
                states.append(copy.deepcopy(game.board.reshape((-1))) * -1)
                labels[-1][0] = player * -1
                if check_gap_consecutive(diagonals(game.board), consecutive=3) == 1 or check_gap_consecutive(antidiagonals(game.board), consecutive=3) == 1:
                    labels[-1][2] += 1
            else:
                states.append(copy.deepcopy(game.board.reshape((-1))))
                labels[-1][0] = player 
                if check_gap_consecutive(diagonals(game.board), consecutive=3) == -1 or check_gap_consecutive(antidiagonals(game.board), consecutive=3) == -1: 
                    labels[-1][2] += 1
            labels[-1][1] = move 
        game.play_move(player, move)
    labels[-1][2] += 1

    return (np.array(states), np.array(labels))

def read_games(fname):
    X = pd.read_csv(fname, names=["game", "idx", "player", "move", "winner"])
    for _, game in X.groupby('game'):
        yield as_board(game)

X = np.vstack([np.hstack(game) for game in read_games('c4-50k.csv')])
np.save('c4.npy', X)
