from connectfour import Game
import numpy as np 
import pandas as pd
import tensorflow as tf
from tensorflow import keras

SIZE = 7*6
model = keras.models.load_model('kerasmodel')
# model = keras.models.load_model('2layer-kerasmodel')
#model = keras.models.load_model('matrix_kerasmodel')
#model = keras.models.load_model('convolutional_kerasmodel')
#model = keras.models.load_model('winningweight-kerasmodel')
wins = 0
losses = 0
draws = 0
for n in range(0,500):
    game = Game()
    while game.status is None:
        # move, p = game.smart_action(-1)
        #prediction = model.predict(game.board.reshape(-1, 6, 7))
        #prediction = model.predict(game.board.reshape(-1, 6, 7, 1))
        prediction = model.predict(np.hstack(game.board).reshape(-1, SIZE))
        #print(prediction)
        move = np.argmax(prediction)
        game.play_move(-1, move)
        #print(game.board)
        if game.status is not None:
            break
        #prediction = model.predict(np.hstack(game.board).reshape(-1, SIZE) * -1)
        #print(prediction)
        #move = np.argmax(prediction)
        #move = int(input("your move [0-6]: "))
        move, p = game.smart_action(1, legal_only=True, n=50)
        #move = game.random_action(legal_only=True)
        game.play_move(1, move)
    if game.status == -1:
        wins+=1
    elif game.status == 1:
        losses+=1
    else:
        draws+=1

    print("game: ",n, "total wins: ", wins, "total losses: ", losses, "total draws: ", draws, " win-rate: ", wins/(wins+losses))

