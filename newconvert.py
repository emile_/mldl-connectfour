import numpy as np
import pandas as pd
from connectfour import Game

def as_board(states):
    # boards = np.empty((len(moves), 6, 7))
    # moves = np.zeros((len(moves), 7))

    boards = []
    moves = []
    X_index = 0
    y_index = 0

    game = Game()
    for _,i in states.iterrows():
        idx, player, move, winner = i['idx'], i['player'], i['move'], i['winner']
        game.play_move(player, move)
        if X_index == y_index :
            if player != winner:
                if winner > 0:
                    boards.append(game.board * -1)
                else:
                    boards.append(game.board)
                X_index += 1
            else:
                boards.append(np.zeros((6,7)))
                t = np.zeros(7)
                t[move] = 1
                moves.append(t)
        elif X_index > y_index:
            if player == winner:
                t = np.zeros(7)
                t[move] = 1
                moves.append(t)
                y_index += 1
        #moves[idx, move] = 1
    if X_index != y_index:
        boards.pop()
    return boards, moves

def read_games(fname):
    X = pd.read_csv(fname, names=["game", "idx", "player", "move", "winner"])
    boards = []
    moves = []
    for _, game in X.groupby('game'):
        tboards, tmoves = as_board(game)
        boards += tboards
        moves += tmoves
    return np.array(boards), np.array(moves)

# X = np.vstack([np.hstack(game) for game in read_games('c4-50k.csv')])

X, y = read_games('c4-10k.csv')
np.save('board_input.npy', X)
np.save('move_output.npy', y)
